/**
 * 
 * SchoolSmart
 * @description           SchoolSmart
 * @author                James Eggington
 * @modified
 * @version               1.0.0
 * @date                  April 2016
 * 
 */
;(function() {
  /**
   * Definition of the main app module and its dependencies
   */
  angular
    .module('schoolsmart', [
      'ui.router'
    ])
    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider){
      $urlRouterProvider.otherwise('/');
      $stateProvider
	.state('schoolsmart.home', {
	  url: "/",
	  templateUrl: "views/reimbursement/home.html",
	  controller: "HomeCtrl"
	})
    }]);

  /**
   * Run block
   */
  angular
    .module('schoolsmart')
    .run([function(){}]);
})();
